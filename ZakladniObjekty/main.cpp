#include <iostream>

using namespace std;

class Ucitel{
public:
    string m_krestniJmeno;
    string m_prijmeni;
    int m_vek;

    void setKrestniJmeno(string jmeno){
        m_krestniJmeno = jmeno;
    }

    void setPrijmeni(string prijmeni){
        m_prijmeni = prijmeni;
    }

    void setVek(int vek){
        m_vek = vek;
    }

    string getKrestniJmeno(){
        return m_krestniJmeno;
    }

    string getPrijmeni(){
        return m_prijmeni;
    }

    int getVek(){
        return m_vek;
    }

    void printInfo(){
        cout<<"Jmeno: "<<m_krestniJmeno<<endl;
        cout<<"Prijmeni: "<<m_prijmeni<<endl;
        cout<<"VekL: "<<m_vek<<endl;
    }
};

class Kurz{
public:
    string m_nazev;
    int m_maxPocetStudentu;

    void setNazev(string nazev){
        m_nazev = nazev;
    }

    void setMaxPocetStudentu(int maxPocetStudentu){
        m_maxPocetStudentu = maxPocetStudentu;
    }

    string getNazev(){
        return m_nazev;
    }

    int getMaxPocetStudentu(){
        return m_maxPocetStudentu;
    }

    void printInfo(){
        cout<<"Nazev: "<<m_nazev<<endl;
        cout<<"Maximalni pocet studentu: "<<m_maxPocetStudentu<<endl;
    }
};


int main(int argc, char *argv[])
{
    Ucitel* ucitel1 = new Ucitel();
    Ucitel* ucitel2 = new Ucitel();
    Kurz* kurz = new Kurz();

    ucitel1->setKrestniJmeno("Ondrej");
    ucitel1->setPrijmeni("Svehla");
    ucitel1->setVek(27);

    ucitel2->setKrestniJmeno("Jan");
    ucitel2->setPrijmeni("Strom");
    ucitel2->setVek(40);

    kurz->setNazev("Zaklady objektove orientovaneho navrhu");
    kurz->setMaxPocetStudentu(100);

    cout<<"Ucitel 1"<<" "<<ucitel1->getPrijmeni()<<endl;
    ucitel1->printInfo();

    cout<<"Ucitel 2"<<" "<<ucitel2->getPrijmeni()<<endl;
    ucitel2->printInfo();

    cout<<"Kurz"<<endl;
    kurz->printInfo();

    delete ucitel1;
    delete ucitel2;
    delete kurz;

    return 0;
}
