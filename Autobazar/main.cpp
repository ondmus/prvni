#include <iostream>
using namespace std;

class Auto{
  public:

    string m_SPZ;
    int m_cena;
    string m_znacka;
    int m_pocetKm;

    //konstruktory
    Auto(string znacka, string SPZ, int cena, int pocetKm){
        osetriZnacku(znacka);
        osetriSPZ(SPZ);
        osetriCenu(cena);
        osetriPocetKm(pocetKm);
    }

    Auto(string znacka, string SPZ, int cena){
        osetriZnacku(znacka);
        osetriSPZ(SPZ);
        osetriCenu(cena);
        m_pocetKm = 0;
    }

    //destruktory
    ~Auto(){
        cout<<"Mazu auto s SPZ: "<<m_SPZ<<endl;
    }

    //metody
    void setZnacka(string znacka){
        osetriZnacku(znacka);
    }

    void setSPZ(string SPZ){
        osetriSPZ(SPZ);
    }

    void setCena(int cena){
        osetriCenu(cena);
    }

    void setPocetKm(int pocetKm){
        osetriPocetKm(pocetKm);
    }

    string getZnacka(){
        return m_znacka;
    }

    string getSPZ(){
        return m_SPZ;
    }

    int getCena(){
        return m_cena;
    }

    int getPocetKm(){
        return m_pocetKm;
    }

    void printInfo(){
        cout<<"Znacka: "<<m_znacka<<endl;
        cout<<"SPZ: "<<m_SPZ<<endl;
        cout<<"Cena: "<<m_cena<<endl;
        cout<<"Pocet km: "<<m_pocetKm<<endl;
    }

    void pujcAuto(int km){
        if (km >= 0){
            int cenaZaKilometry = km * 10;

            if ((m_cena - cenaZaKilometry) >= 0){
                m_pocetKm += km;
                m_cena -= cenaZaKilometry;
            } else {
                cout<<"Nejde uz pujcit, dostal by ses do zaporu"<<endl;
            }
        } else {
            cout<<"Ujel jsi zaporny pocet km"<<endl;
        }
    }

    //metdy pro osetreni vstupu
    void osetriSPZ(string SPZ){
        if(SPZ.size() == 8){
            m_SPZ = SPZ;
        } else {
            cout<<"Spatny tvar SPZ"<<endl;
        }
    }

    void osetriZnacku(string znacka){
        if (znacka.size() > 0){
            m_znacka = znacka;
        } else {
            cout<<"Spatny nazev"<<endl;
        }
    }

    void osetriCenu(int cena){
        if (cena > 0){
            m_cena = cena;
        } else {
            cout<<"Mala cena"<<endl;
        }
    }

    void osetriPocetKm(int km){
        if (km >= 0) {
            m_pocetKm = km;
        } else {
            cout<<"Maly pocet km"<<endl;
        }
    }
};

int main(int argc, char *argv[])
{
    Auto* a = new Auto("Audi", "2H6 5897", 100, 0);
    a->printInfo();

    a->pujcAuto(10);

    a->printInfo();

    a->pujcAuto(-10);
    a->pujcAuto(12);

    delete a;

    return 0;
}
