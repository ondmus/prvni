#include <iostream>
using namespace std;

class Nastaveni{
private:
    static int s_obtiznost;

public:
    static void setObtiznost (int obtiznost){
        s_obtiznost = obtiznost;
    }

    static int getObtiznost (){
        return s_obtiznost;
    }
};

class Brneni{
private:
    int m_bonus;

public:
    Brneni(int bonus){
        m_bonus = bonus;
    }

    void setBonus(int bonus){
        m_bonus = bonus;
    }

    int getBonus (){
        return m_bonus;
    }
};

class Postava{
private:
    int m_obrana;
    int m_utok;
    int m_zivot;

public:
    Postava(int obrana, int utok){
        m_obrana = obrana;
        m_utok = utok;
        m_zivot = 100;
    }

    void printInfo(){
        cout<<"Obrana: "<<m_obrana<<endl;
        cout<<"Utok: "<<m_utok<<endl;
        cout<<"Zivot: "<<m_zivot<<endl;
    }

    void setObrana(int obrana){
        m_obrana = obrana;
    }

    void setUtok(int utok){
        m_utok = utok;
    }

    void setZivot(int zivot){
        m_zivot = zivot;
    }

    int getObrana (){
        return m_obrana;
    }

    int getUtok (){
        return m_utok;
    }

    int getZivot (){
        return m_zivot;
    }

    void seberBrneni(Brneni* co){
        m_obrana += co->getBonus();
    }

    void bojuj(Postava* sKym){
        int zivot;

        if((m_utok > sKym->getObrana()) or (m_obrana < sKym->getUtok())){
            while ((m_zivot > 0) and (sKym->getZivot()>0)){
               if(m_utok > sKym->getObrana()){
                   zivot = sKym->getZivot()-(m_utok - sKym->getObrana());
                   sKym->setZivot(zivot);
               }

               if (m_obrana < (sKym->getUtok()*Nastaveni::getObtiznost())){
                   m_zivot-=((sKym->getUtok()*Nastaveni::getObtiznost()) - m_obrana);
               }
             }
           } else{
                cout<<"Nemuze rpobehnout boj!"<<endl;
            }

    }
};

int Nastaveni::s_obtiznost = 1;

int main(int argc, char *argv[])
{
    Brneni* stit = new Brneni(10);
    Postava* rytir = new Postava(20, 30);
    Postava* drak = new Postava(15,35);

    rytir->printInfo();
    drak->printInfo();

    drak->seberBrneni(stit);

    rytir->bojuj(drak);

    rytir->printInfo();
    drak->printInfo();

    delete stit;
    delete rytir;
    delete drak;

    return 0;
}
