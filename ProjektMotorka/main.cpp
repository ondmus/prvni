#include <iostream>
using namespace std;

class Motorka{
private:
    string m_SPZ;
    int m_vykon;
    float m_pocetKM;

public:
    Motorka(string SPZ, int vykon, float km){
        kontrolaSPZ(SPZ);
        kontrolaKM(km);
        kontrolaVykon(vykon);
    }

    Motorka(string SPZ){
        kontrolaSPZ(SPZ);
        m_vykon = 0;
        m_pocetKM = 0;
    }

    ~Motorka(){
        cout<<"Mazu motorku s SPZ: "<<m_SPZ<<endl;
    }

    void setSPZ(string SPZ){
        kontrolaSPZ(SPZ);
    }

    void setVykon(int vykon){
        kontrolaVykon(vykon);
    }

    void setKm(float km){
        kontrolaKM(km);
    }

    string getSPZ(){
        return m_SPZ;
    }

    int getVykon(){
        return m_vykon;
    }

    float getPocetKm(){
        return m_pocetKM;
    }

    void printInfo(){
        cout<<"SPZ: "<<m_SPZ<<endl;
        cout<<"Vykon: "<<m_vykon<<endl;
        cout<<"Pocet ujetych km: "<<m_pocetKM<<endl;
    }

private:
    void kontrolaSPZ(string SPZ){
        if (SPZ.size() == 8) {
            m_SPZ = SPZ;
        } else {
            cout<<"spatna spz"<<endl;
        }
    }

    void kontrolaVykon(int vykon){
        if (vykon > 0) {
            m_vykon = vykon;
        } else {
            cout<<"spatny vykon"<<endl;
        }
    }

    void kontrolaKM(float km){
        if (km >= 0) {
            m_pocetKM = km;
        } else {
            cout<<"spatne km"<<endl;
        }
    }
};

int main(int argc, char *argv[])
{
    Motorka* m1 = new Motorka("2K6 4587", 100, 20);
    Motorka* m2 = new Motorka("4P8 4536");

    m1->printInfo();
    m2->printInfo();

    cout<<"Zadej km"<<endl;
    float km;

    cin>>km;

    m2->setKm(km);

    cout<<"celkovy pocet km: "<<endl;
    km = m1->getPocetKm() + m2->getPocetKm();
    cout<<km<<endl;

    delete m1;
    delete m2;

    return 0;
}
